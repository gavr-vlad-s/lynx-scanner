/*
    File:    test_lynx_scaner.h
    Created: 12 March 2019 at 08:32 Moscow time
    Author:  Гаврилов Владимир Сергеевич
    E-mails: vladimir.s.gavrilov@gmail.com
             gavrilov.vladimir.s@mail.ru
             gavvs1977@yandex.ru
*/

#ifndef TEST_LYNX_SCANER_H
#define TEST_LYNX_SCANER_H
#   include <memory>
#   include "../include/lynx_scaner.h"
void test_lynx_scaner(const std::shared_ptr<lynx_scanner::Scanner>& lynxsc);
#endif